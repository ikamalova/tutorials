# Working with SolrCloud
SolrCloud is extention for Solr and through Java API it lets you not only add/update/delete documents n collections, but also create collection and manage schema and configuration for concrete collection.

### Start SolrCloud locally
To start extension locally it's enough to switch on cloud mode and accept all prompts:
```bash
solr -e cloud
```
<details><summary>result:</summary><p>

```
Ok, let's start up 2 Solr nodes for your example SolrCloud cluster.
Please enter the port for node1 [8983]:

Please enter the port for node2 [7574]:

Creating Solr home directory C:\solr-8.6.0\example\cloud\node1\solr
Cloning C:\solr-8.6.0\example\cloud\node1 into
   C:\solr-8.6.0\example\cloud\node2

Starting up Solr on port 8983 using command:
"C:\solr-8.6.0\bin\solr.cmd" start -cloud -p 8983 -s "C:\solr-8.6.0\example\cloud\node1\solr"

Waiting up to 30 to see Solr running on port 8983
Started Solr server on port 8983. Happy searching!

Starting up Solr on port 7574 using command:
"C:\solr-8.6.0\bin\solr.cmd" start -cloud -p 7574 -s "C:\solr-8.6.0\example\cloud\node2\solr" -z localhost:9983

Waiting up to 30 to see Solr running on port 7574
Started Solr server on port 7574. Happy searching!
....
```
</p>
</details>

By default, it will create two nodes: on 8983 and 7574 ports, and also zookeeper on the port 9983

To create SolrCloudClient, it needs to define zkHost and zkChRoot:
```java
String zkHost = "localhost:9983";  
List<String> zkHosts = Collections.singletonList(zkHost);  
Optional<String> zkChRoot = Optional.of("/");  
  
CloudSolrClient cloudSolrClient = new CloudSolrClient.Builder(zkHosts, zkChRoot)  
        .withConnectionTimeout(30000)  
        .withSocketTimeout(30000)  
        .build();
```

To update configuration, you need to create zookeeper client, define path to files with your **schema.xml** and **solrconfig.xml** configuration files and use method ```uploadConfig```:

```java
ZkClientClusterStateProvider zkClientCluster = new ZkClientClusterStateProvider(zkHost);  
Path configPath = getFile(System.getProperty("user.dir")).toPath().resolve("conf");  
String configName = "testconfig";  
zkClientCluster.uploadConfig(configPath, configName);
```
To create the collection, we need to define the name of collection, provide configuration that we saved in Zookeeper, and define the number of shard and replica:
```java
String collectionName = "newCollection";  
int numShards = 1;  
int numReplicas = 1;  
CollectionAdminResponse rsp = CollectionAdminRequest.createCollection(collectionName, configName, numShards, numReplicas)  
        .process(cloudSolrClient);  
System.out.println(rsp.isSuccess());
```
Rules of thumb for number of shards and replicas:
```
numShards must be > 0
nrtReplicas + tlogReplicas must be > 0
```
It also possible to check if collection already exist:
```java
ClusterState.CollectionRef state = zkClientCluster.getState(collectionName);  
System.out.println(state);
``` 
If collection doesn't exist, the values of state is ```null```,  otherwise it will bring something like this: ```LazyCollectionRef(newCollection)```

It also possible to delete collection:
```java
CollectionAdminRequest.deleteCollection(collectionName).process(cloudSolrClient);
```
To check that collection was successfully created, it possible to execute simple request:
```java
QueryRequest req = new QueryRequest(new SolrQuery("*:*"));  
QueryResponse queryResponse = req.process(cloudSolrClient, collectionName);  
System.out.println(queryResponse.getStatus());
```